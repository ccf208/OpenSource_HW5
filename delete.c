#include <stdio.h>
#include <string.h>

#include "phone.h"

extern int rmBuffer;
extern int userNo;

int Delete(book * head)
{
    int i = 0;
    char enterName[10];
    int inserterror = 0;

    book * current = head;
    book * temp_node = NULL;

    printf("\n4.Delete\n");
    printf("Please Enter Name to Delete: ");
    scanf("%9s", enterName); while((rmBuffer= getchar()) != '\n' && rmBuffer != EOF);

    while(current->next != NULL){

        if (strcmp(current->next->UserName, enterName) == 0) {
            printf("%s Delete Complete!\n", enterName);
            inserterror = 0;
            break;
        } else {
            inserterror = 1;
        }
        current = current->next;
    }

    if (current->next != NULL && inserterror == 0){
        temp_node = current->next;
        current->next = temp_node->next;
        free(temp_node);
        return 0;
    }
    else{
        printf("Please enter right name.\n");
        return 0;
    }

}
