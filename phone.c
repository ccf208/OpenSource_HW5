#include <stdio.h>
#include <stdlib.h>

#include "phone.h"

char password[] = "qwer1234";

void Register(book * head);
void Allprint(book * head);
void Personalprint(book * head);
int Delete(book * head);
void FileOpen(book * head);
void SaveFile(book * head);

int userNo = 0;
int rmBuffer=0;

int main(int argc, char* argv[])
{

    int service;

    book * head = NULL;
    head = malloc(sizeof(book));
    if (head == NULL) {
        return 1;
    }
    memset(head->UserName,0,10);
    memset(head->UserNumber,0,15);
    head->next = NULL;

    FileOpen(head);

    do
    {
        printf("\n***Phone number Management***\n");
        printf("\n1.Register    2.Search All    3.Search Name    4.Delete    5.Exit\n");
        printf("\nSelect Menu: ");
        scanf("%d", &service); while((rmBuffer= getchar()) != '\n' && rmBuffer != EOF);

        if(service <= 5 && service >= 1)
        {
            switch (service)
            {
                case 1: Register(head); break;
                case 2: Allprint(head); break;
                case 3: Personalprint(head); break;
                case 4: Delete(head); break;
            }
        }
        else
        {
            printf("\nIncorrect Insert.\n");
        }

    }while (service != 5);

    printf("\nStop program.\n");
    SaveFile(head);
    free(head);

    return 0;
}

void FileOpen(book * head) {

    FILE *fp;
    head->next = NULL;
    book * current = head;
    book x;
    fp = fopen("phone.dat", "rb");

    if (fp == NULL){
        printf("File loading Error.\n");
    }
    else {
        while (fread(&x, sizeof(book), 1, fp) != 0){
            x.next = NULL;
            current->next = malloc(sizeof(book));
            *(current->next) = x;
            current = current->next;
        }
        printf("File loading Success!\n");
    }
    fclose(fp);
}

void SaveFile(book * head) {

    FILE *fp;
    book * current = head;
    fp = fopen("phone.dat", "wb");

    while (current != NULL){
        fwrite(current->next, sizeof(book), 1, fp);
        current = current->next;
    }
    printf("File saved completely!\n");
    fclose(fp);

}
