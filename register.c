#include <stdio.h>
#include <string.h>

#include "phone.h"

extern char password[];
extern int rmBuffer;
extern int userNo;

void Register(book * head)
{
    book * current = head;

    while (current->next != NULL){
        current = current->next;
    }

    static int passwordError = 1;
    char Inpassword[13];
    int passwordmark = 0;

    printf("\n1.Register\n");
    printf("Please Enter Password.\n");

    if(passwordError == 3){
        printf("\nPassword error 3. Can't be register.\n");
    }
    else {
        do{
            printf("Password: ");
            scanf("%12s", Inpassword);
            while((rmBuffer = getchar()) != '\n' && rmBuffer != EOF);

            if((strcmp(Inpassword, password)) == 0){
                passwordError = 1;

                current->next = malloc(sizeof(book));

                printf("Name to Register: ");
                scanf("%9s", current->next->UserName); while((rmBuffer = getchar()) != '\n' && rmBuffer != EOF);
                printf("Number to Register: ");
                scanf("%14s", current->next->UserNumber); while((rmBuffer= getchar()) != '\n' && rmBuffer != EOF);
                printf("Registered Complete!\n");

                current->next->next = NULL;

                passwordmark = 1;
            } else {
                if (passwordError == 3){
                    printf("\nPassword error 3. Can't be register.\n"); break;
                }
                printf("\nPassword error in %d.\n", passwordError);
                passwordError++;
            }
        } while (passwordmark != 1);
    }

}
