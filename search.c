#include <stdio.h>
#include <string.h>

#include "phone.h"

extern int rmBuffer;
extern int userNo;

void Personalprint(book * head)
{
    book * current = head;
    char searchName[10];

    printf("\n3.Search Name");
    printf("\nName for search: ");
    scanf("%9s", searchName); while((rmBuffer= getchar()) != '\n' && rmBuffer != EOF);

    while (current->next != NULL){
        if (strcmp(current->next->UserName, searchName) == 0){
            printf("\n%s  %s\n", current->next->UserName, current->next->UserNumber);
            break;
        } current = current->next;
    }
}


void Allprint(book * head)
{
    printf("\n2.Search All\n");
    printf("\n<< All Numbers >>\n");

    book * current = head;
    while (current->next != NULL){
        printf("%s  %s\n", current->next->UserName, current->next->UserNumber);
        current = current->next;
    }
}
